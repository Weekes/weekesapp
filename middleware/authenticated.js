export default function ({ store, redirect }) {
    // If the current user is not authenticated redirect them to the login.
    if (store.state.auth.loggedIn) {
        return redirect({ name: 'dashboard' });
    }
    return true;
}
