export default function ({ store, redirect }) {
    // If the current user is authenticated redirect them to the Dashboard
    if (!store.state.auth.loggedIn) {
        return redirect({ name: 'login' });
    }
    return true;
}
