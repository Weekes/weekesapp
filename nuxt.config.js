export default {
    mode: 'universal',
    dev: (process.env.NODE_ENV !== 'production'),
    modern: true,

    /*
    ** Headers of the page
    */
    head: {
        title: 'Weekes.io | Laravel & Vue Developer',
        meta: [
            { charset: 'utf-8' },
            { name: 'viewport', content: 'width=device-width, initial-scale=1' },
            { hid: 'description', name: 'description', content: process.env.npm_package_description || '' }
        ],
        link: [
            { rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' }
        ]
    },

    /*
    ** Customize the progress-bar color
    */
    loading: {
        color: '#fff'
    },

    /*
    ** Global CSS
    */
    css: [
        '@/assets/fonts/oswald.css',
        '@/assets/fonts/roboto.css',
        '@fortawesome/fontawesome-svg-core/styles.css',
    ],

    /*
    ** Plugins to load before mounting the App
    */
    plugins: [
        '@/plugins/config',
        '@/plugins/i18n',
        '@/plugins/fontawesome',
    ],

    /*
    ** Nuxt.js dev-modules
    */
    buildModules: [
        '@nuxtjs/tailwindcss',
    ],
    tailwindcss: {
        configPath: '~/config/tailwind.config.js',
        cssPath: '~/assets/css/tailwind.css',
        purgeCSSInDev: false,
        exposeConfig: false
    },

    /*
    ** Nuxt.js modules
    */
    modules: [
        '@nuxtjs/axios',
        '@nuxtjs/auth',
        '@nuxtjs/pwa',
    ],

    axios: {
        baseURL: process.env.NODE_ENV !== 'production' ? 'http://localhost:8000/api' : 'https://weekes.io/api/'
    },

    auth: {
        strategies: {
            local: {
                endpoints: {
                    login: { url: '/auth/login', method: 'post', propertyName: 'access_token' },
                    logout: { url: '/auth/logout', method: 'post' },
                    user: { url: '/auth/user', method: 'get', propertyName: 'user' }
                },
            }
        }
    },

    /*
    ** Build configuration
    */
    build: {
        /*
        ** You can extend webpack config here
        */
        extend (config, ctx) {
        }
    }
}
