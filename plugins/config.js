import endpoints from '../endpoints';

export default (nuxt, inject) => {
    inject('endpoints', endpoints);
};
