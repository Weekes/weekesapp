import Vue from 'vue';

import { library } from '@fortawesome/fontawesome-svg-core';
import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome';
import { faLaravel } from '@fortawesome/free-brands-svg-icons';
import { faVuejs } from '@fortawesome/free-brands-svg-icons';
import { faWordpress } from '@fortawesome/free-brands-svg-icons';
import { faDigitalOcean } from '@fortawesome/free-brands-svg-icons';
import { faUbuntu } from '@fortawesome/free-brands-svg-icons';
import { faGitAlt } from '@fortawesome/free-brands-svg-icons';
import { faCpanel } from '@fortawesome/free-brands-svg-icons';
import { faStripe } from '@fortawesome/free-brands-svg-icons';
import { faPoundSign } from '@fortawesome/pro-light-svg-icons';
import { faStopwatch } from '@fortawesome/pro-light-svg-icons';
import { faTools } from '@fortawesome/pro-light-svg-icons';
import { faChargingStation } from '@fortawesome/pro-light-svg-icons';
import { faHome } from '@fortawesome/pro-light-svg-icons';
import { faTicketAlt } from '@fortawesome/pro-light-svg-icons';
import { faChartPie } from '@fortawesome/pro-light-svg-icons';
import { faUsers } from '@fortawesome/pro-light-svg-icons';
import { faProjectDiagram } from '@fortawesome/pro-light-svg-icons';
import { faUser } from '@fortawesome/pro-light-svg-icons';
import { faCogs } from '@fortawesome/pro-light-svg-icons';
import { faCircleNotch } from '@fortawesome/pro-light-svg-icons';

// Todo call all icons we want to use from an icons array to make this cleaner

// Brands
library.add(faLaravel, faVuejs, faWordpress, faDigitalOcean, faUbuntu, faGitAlt, faCpanel, faStripe);
library.add(faPoundSign, faStopwatch, faTools, faChargingStation, faHome, faTicketAlt, faChartPie, faUsers, faProjectDiagram, faCircleNotch, faUser, faCogs);

Vue.component('font-awesome-icon', FontAwesomeIcon);
