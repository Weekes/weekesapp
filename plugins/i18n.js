import Vue from 'vue';
import VueI18n from 'vue-i18n';

const en = require('../locales/en.json');

Vue.use(VueI18n);

export default ({ app }) => {
    app.i18n = new VueI18n({
        locale: 'en',
        fallbackLocale: 'en',
        messages: {
            en,
        },
    });

    app.i18n.path = (link) => {
        if (app.i18n.locale === app.i18n.fallbackLocale) {
            return `/${link}`;
        }

        return `/${app.i18n.locale}/${link}`;
    };
};
